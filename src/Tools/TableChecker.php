<?php

namespace YuryBoyka\Architech\Tools;

use YuryBoyka\Architech\Exceptions\CreateDefinitionException as Exception;

class TableChecker
{
    const
        DETECT_CREATE_ROW_PATTERN = '/CREATE TABLE (\w+)/',
        DETECT_FOREIGN_KEY_PATTERN = '/foreign key/i',
        COLUMN_PATTERN = '/^(?P<name>.+?) '
    . '(?P<type>\w+(\(.+?\))?( unsigned)?( null)?[ $]?)'
    . '(COLLATE (?P<collate>\w+)[ $]?)?'
    . '((?P<not_null>not null)[ $]?)?'
    . '((?P<default>default ((\'.*\')|([^ ]+)))[ $]?)?'
    . '((?P<ai>auto_increment)[ $]?)?'
    . '((?P<index>primary key|unique)[ $]?)?'
    . '/ui',
        COLUMN_TYPE_PATTERN = '/(?P<type>\w+)(?P<value>\(.+?\))?(?P<unsigned> unsigned)?(?P<null> null)?/ui',
        INDEX_PATTERN = '/^((?P<primary>primary) )?'
    . '((?P<unique>unique) )?'
    . '(key|index) ((?P<name>\w+) )?'
    . '\((?P<fields>.+?)\)/ui',
        FOREIGN_KEY_PATTERN = '/^(constraint (?P<name>\w+) )?'
    . 'foreign key \((?P<field>\w+)\) '
    . 'references (?P<foreign_table>\w+) '
    . '\((?P<foreign_field>\w+)\)[ $]?'
    . '(on update (?P<on_update>set null|set default|restrict|cascade|no action)[ $]?)?'
    . '(on delete (?P<on_delete>set null|set default|restrict|cascade|no action))?'
    . '/iu';

    /**
     * @var array
     */
    private static $defaultNumericLengths = [
        'tinyint'   => 3,
        'smallint'  => 5,
        'mediumint' => 8,
        'int'       => 10,
        'bigint'    => 19
    ];

    /**
     * @var array
     */
    private static $notQuotedDefaultValues = ['NULL', 'CURRENT_TIMESTAMP'];

    /**
     * @param string $createDefinition
     * @return array
     * @throws Exception
     */
    public static function parseCreateTableDefinition(string $createDefinition): array
    {
        $table = ['name' => '', 'columns' => [], 'indexes' => []];
        /* TODO: Переписать обработку create_definition через регулярное выражение для возможности парсинга из одной строки */
        $rows = self::getRowsFromCreateDefinition($createDefinition);

        foreach ($rows as $key => $row) {
            $row = trim(str_replace('`', '', trim($row)), ',');

            if (0 === $key && preg_match(self::DETECT_CREATE_ROW_PATTERN, $row, $matches)) {
                $table['name'] = $matches[1];
            } elseif (preg_match(self::DETECT_FOREIGN_KEY_PATTERN, $row)) {
                $foreignKey = self::parseForeignKeyFromDefinition($table['name'], $row);
                $table['indexes'][$foreignKey['name']] = $foreignKey;
            } elseif (preg_match(self::INDEX_PATTERN, $row, $matches)) {
                $index = self::parseIndexFromPatternMatches($table['name'], $matches);
                $table['indexes'][$index['name']] = $index;
            } elseif (preg_match(self::COLUMN_PATTERN, $row, $matches)) {
                $columnParams = self::parseColumnDefinitionFromPatternMatches($matches);
                $table['columns'][$columnParams['name']] = $columnParams['definition'];

                if (array_key_exists('index', $columnParams)) {
                    $indexName = self::getIndexNameByTableNameAndParams($table['name'], $columnParams['index']);
                    $table['indexes'][$indexName] = [
                        'name' => $indexName,
                        'type' => $columnParams['index']['type'],
                        'fields' => $columnParams['index']['fields']
                    ];
                }
            }
        }

        return $table;
    }

    /**
     * @param string $createDefinition
     * @return array
     */
    private static function getRowsFromCreateDefinition(string $createDefinition): array
    {
        $rows = explode("\n", trim($createDefinition));
        return array_slice($rows, 0, -1);
    }

    /**
     * @param string $tableName
     * @param string $definition
     * @return array
     * @throws Exception
     */
    private static function parseForeignKeyFromDefinition(string $tableName, string $definition): array
    {
        if (!preg_match(self::FOREIGN_KEY_PATTERN, $definition, $matches)) {
            throw new Exception(sprintf(Exception::INVALID_FOREIGN_KEY_DEFINITION, $definition));
        }

        return [
            'type'          => 'foreign',
            'name'          => empty($matches['name']) ? "{$tableName}_{$matches['field']}_fk" : $matches['name'],
            'fields'        => [$matches['field']],
            'foreign_table' => $matches['foreign_table'],
            'foreign_field' => $matches['foreign_field'],
            'on_update'     => $matches['on_update'] ?? '',
            'on_delete'     => $matches['on_delete'] ?? ''
        ];
    }

    /**
     * @param string $tableName
     * @param array $matches
     * @return array
     */
    private static function parseIndexFromPatternMatches(string $tableName, array $matches): array
    {
        $index = [
            'fields' => empty($matches['fields']) ? [$matches['field']] : explode(',', $matches['fields'])
        ];

        foreach ($index['fields'] as &$field) {
            $field = trim($field);
        }

        if (!empty($matches['primary'])) {
            $index += ['name' => 'PRIMARY', 'type' => 'primary'];
        } else  {
            $index['type'] = empty($matches['unique']) ? 'index' : 'unique';
            $index['name'] = empty($matches['name'])
                ? self::getIndexNameByTableNameAndParams($tableName, $index)
                : $matches['name'];
        }

        return $index;
    }

    /**
     * @param string $tableName
     * @param array $params
     * @return string
     */
    private static function getIndexNameByTableNameAndParams(string $tableName, array $params): string
    {
        if ('primary' === $params['type']) {
            return 'PRIMARY';
        }

        $indexName = "{$tableName}_" . implode('_', $params['fields']) . "_{$params['type']}";

        return strlen($indexName) > 63 ? substr($indexName, 0, 63) : $indexName;
    }

    /**
     * @param array $matches
     * @return array
     * @throws Exception
     */
    private static function parseColumnDefinitionFromPatternMatches(array $matches): array
    {
        $column = [];
        $matches += ['not_null' => null, 'default' => null, 'ai' => null, 'index' => null];
        $columnParams = [
            'name'     => $matches['name'],
            'type'     => self::getFormatColumnType($matches['type']),
            'not_null' => (bool)$matches['not_null'],
            'default'  => preg_match('/DEFAULT (.+)/', $matches['default'], $defaultMatches)
                ? trim($defaultMatches[1], "'")
                : null,
            'ai'       => (bool)$matches['ai']
        ];
        $column['name'] = $columnParams['name'];
        $column['definition'] = self::getColumnDefinitionFromParams($columnParams);

        if ($matches['index']) {
            $column['index'] = [
                'type'   => ('unique' === trim(strtolower($matches['index']))) ? 'unique' : 'primary',
                'fields' => [trim($columnParams['name'])]
            ];
        }

        return $column;
    }

    /**
     * @param string $type
     * @return string
     * @throws Exception
     */
    private static function getFormatColumnType(string $type): string
    {
        if (!preg_match(self::COLUMN_TYPE_PATTERN, $type, $matches)) {
            throw new Exception(sprintf(Exception::INVALID_COLUMN_TYPE, $type));
        }

        $type = strtolower($matches['type']);
        $value = $matches['value'] ?? '';
        $unsigned = empty($matches['unsigned']) ? '' : ' UNSIGNED';
        $null = empty($matches['null']) ? '' : ' NULL';

        return empty($value) && array_key_exists($type, self::$defaultNumericLengths)
            ? strtoupper($type) . '(' . self::$defaultNumericLengths[$type] . "){$unsigned}{$null}"
            : strtoupper($type) . "{$value}{$unsigned}{$null}";
    }

    /**
     * @param array $params
     * @return string
     */
    private static function getColumnDefinitionFromParams(array $params): string
    {
        return "`{$params['name']}` {$params['type']}"
            . ($params['not_null'] ? ' NOT NULL' : '')
            . (is_null($params['default']) ? '' : ' DEFAULT ' . self::formatDefaultValue($params['default']))
            . ($params['ai'] ? ' AUTO_INCREMENT' : '');
    }

    /**
     * @param string $defaultValue
     * @return string
     */
    private static function formatDefaultValue(string $defaultValue): string
    {
        if (in_array(mb_strtolower($defaultValue), ['current_timestamp', 'current_timestamp()'])) {
            return 'CURRENT_TIMESTAMP()';
        }

        $defaultValue = trim($defaultValue, "'");
        return in_array($defaultValue, self::$notQuotedDefaultValues) ? $defaultValue : "'{$defaultValue}'";
    }

    /**
     * @param array $tableParams
     * @return array
     */
    public static function getCreateTableDefinition(array $tableParams): array
    {
        $eol = PHP_EOL;
        $creatingRows = $tableParams['columns'];
        $indexes = [];

        foreach ($tableParams['indexes'] as $index) {
            if ('foreign' !== $index['type']) {
                $creatingRows[] = self::getCreateIndexDefinition($index);
            } else {
                $indexes[] = "ALTER TABLE `{$tableParams['name']}` ADD " . self::getCreateIndexDefinition($index) . ';';
            }
        }

        return [
            'table' => "CREATE TABLE `{$tableParams['name']}` ({$eol}\t" . implode(",{$eol}\t", $creatingRows) . "{$eol});",
            'indexes' => $indexes
        ];
    }

    /**
     * @param array $params
     * @return string
     */
    private static function getCreateIndexDefinition(array $params): string
    {
        $fields = '`' . implode('`, `', $params['fields']) . '`';

        switch($params['type']) {
            case 'primary':
                return "PRIMARY KEY ({$fields})";
            case 'unique':
                return "UNIQUE KEY `{$params['name']}` ({$fields})";
            case 'foreign':
                return "CONSTRAINT `{$params['name']}` FOREIGN KEY ({$fields}) "
                    . "REFERENCES `{$params['foreign_table']}` (`{$params['foreign_field']}`)"
                    . (empty($params['on_update']) ? '' : " ON UPDATE {$params['on_update']}")
                    . (empty($params['on_delete']) ? '' : " ON DELETE {$params['on_delete']}");
            default:
                return "KEY `{$params['name']}` ({$fields})";
        }
    }

    /**
     * @param array $newTable
     * @param array $existTable
     * @return array
     */
    public static function compareTables(array $newTable, array $existTable): array
    {
        $alterString = "ALTER TABLE `{$newTable['name']}`";
        $alterTable = [];
        $deleteIndexes = [];
        $createIndexes = [];

        $existColumns = array_keys($existTable['columns']);
        $newColumns = array_keys($newTable['columns']);
        $existIndexes = array_keys($existTable['indexes']);
        $newIndexes = array_keys($newTable['indexes']);

        $columnsForDeleting = array_diff($existColumns, $newColumns);
        $columnsForCreating = array_diff($newColumns, $existColumns);
        $columnsForModifying = array_intersect($newColumns, $existColumns);
        $indexesForDeleting = array_diff($existIndexes, $newIndexes);
        $indexesForCreating = array_diff($newIndexes, $existIndexes);
        $indexesForModifying = array_intersect($newIndexes, $existIndexes);

        foreach ($columnsForDeleting as $columnName) {
            $alterTable[] = "DROP COLUMN `{$columnName}`";
        }

        foreach ($columnsForCreating as $columnName) {
            $alterTable[] = "ADD COLUMN {$newTable['columns'][$columnName]}";
        }

        foreach ($columnsForModifying as $columnName) {
            if ($newTable['columns'][$columnName] !== $existTable['columns'][$columnName]) {
                $alterTable[] = "MODIFY COLUMN {$newTable['columns'][$columnName]}";
            }
        }

        foreach ($indexesForDeleting as $indexName) {
            $deleteIndexes[] = "{$alterString} " . self::getDropIndexDefinition($existTable['indexes'][$indexName]) . ';';
        }

        foreach ($indexesForCreating as $indexName) {
            $createIndexes[] = "{$alterString} ADD "
                . self::getCreateIndexDefinition($newTable['indexes'][$indexName]) . ';';
        }

        foreach ($indexesForModifying as $indexName) {
            ksort($existTable['indexes'][$indexName]);
            ksort($newTable['indexes'][$indexName]);

            if ($existTable['indexes'][$indexName] !== $newTable['indexes'][$indexName]) {
                $deleteIndexes[] = "{$alterString} "
                    . self::getDropIndexDefinition($existTable['indexes'][$indexName]) . ';';
                $createIndexes[] = "{$alterString} ADD "
                    . self::getCreateIndexDefinition($newTable['indexes'][$indexName]) . ';';
            }
        }

        return [
            'alter_table' => empty($alterTable) ? [] : ["{$alterString}\n\t" . implode(",\n\t", $alterTable) . ';'],
            'create_indexes' => $createIndexes,
            'delete_indexes' => $deleteIndexes
        ];
    }

    /**
     * @param array $params
     * @return string
     */
    private static function getDropIndexDefinition(array $params): string
    {
        return 'DROP' . ((in_array($params['type'], ['primary', 'foreign'])) ? ' ' . strtoupper($params['type']) : '')
            . ' KEY' . (('primary' === $params['type']) ? '' : " `{$params['name']}`");
    }
}
