<?php

namespace YuryBoyka\Architech\Exceptions;

use Exception;

class CreateDefinitionException extends Exception
{
    const
        INVALID_COLUMN_DEFINITION      = 'Не удалось разобрать описание поля: %s',
        INVALID_COLUMN_TYPE            = 'Не удалось разобрать тип поля: %s',
        INVALID_FOREIGN_KEY_DEFINITION = 'Не удалось разобрать описание внешнего ключа: %s';
}
